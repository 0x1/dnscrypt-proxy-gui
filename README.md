# dnscrypt-proxy gui

![platform](https://img.shields.io/badge/platform-debian%2Flinux-green.svg)
![zenity](https://img.shields.io/badge/zenity-3.30.0-blue.svg)<br/>
<br/>

[![dnscrypt-proxy-gui](https://0x1.gitlab.io/img/blog/dnscrypr-proxy.png)](https://0x1.gitlab.io/blog/security/Dnscrypt-Proxy-Gui/)


#### Gui for [dnscrypt-proxy 2](https://github.com/jedisct1/dnscrypt-proxy) - A flexible DNS proxy, with support for encrypted DNS protocols. <br/>

- Simple gui coded with zenity for help to chose server. 
- To install need root, but to run dnscrypt-proxy, do not run as root (for security and better performance)

###### Run dnscrypt from a non-root user: :

> `sudo setcap 'cap_net_bind_service=+pe' /usr/sbin/dnscrypt-proxy`


#### Install:

- with git

```bash
su
cd /tmp; git clone https://gitlab.com/0x1/dnscrypt-proxy-gui.git
cd dnscrypt-proxy-gui
chmod +x install
./install
```
- with wget

```bash
su
cd /tmp; wget https://gitlab.com/0x1/dnscrypt-proxy-gui/raw/master/install-online
chmod +x install-online
./install-online

```


#### To Active Sandbox :

> `sed -i 's/sandbox="false"/sandbox="true"/' /usr/bin/dns`

For Sandbox i use [firejail](https://github.com/netblue30/firejail) with option [--apparmor](https://gitlab.com/apparmor/apparmor)

> `apt-get install firejail apparmor`

#### Run:

```html
      ██████╗ ███╗   ██╗███████╗
      ██╔══██╗████╗  ██║██╔════╝
      ██║  ██║██╔██╗ ██║███████╗
      ██║  ██║██║╚██╗██║╚════██║
      ██████╔╝██║ ╚████║███████║
      ╚═════╝ ╚═╝  ╚═══╝╚══════╝

           DNScrypt-Proxy Gui
                       by 0x1
 
[Blog]:: https://0x1.gitlab.io
[+][https://level23hacktools.com/]

Usage : dns (to start dnscrypt-proxy gui)
Options : dns or dns -g or dns --gui (Chose one server )
-m or --multi (Chose multiple servers)
-r $NameServer or --resolv $NameServer (Cmd to chose server No gui)
-l or --list (list of servers)
-c or --config (configuration for dnscrypt-proxy)
-h or --help (to see help)
```
#### Requires:

- git or wget (for install)
- dnscrypt-proxy
- zenity
- leafpad  (for option -c {to see configuratiion files}) 
- firejail (for option sandbox )

#### Demo Gif:

![dnscrypt-proxy-gui](https://0x1.gitlab.io/img/blog/dnscrypt-proxy.gif)

